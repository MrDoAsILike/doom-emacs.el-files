;; -*- no-byte-compile: t; -*-
;;; $DOOMDIR/packages.el

;; To install a package with Doom you must declare them here and run 'doom sync'
;; on the command line, then restart Emacs for the changes to take effect -- or
;; use 'M-x doom/reload'.


;; To install SOME-PACKAGE from MELPA, ELPA or emacsmirror:
;(package! some-package)

;;;###Package Name                              ;;;###Comments

(package! company-mode)
(package! company-tabnine)

(package! evil-org)
(package! evil-tutor)                  ;;Evil vim like tutor
(package! evil-commentary)             ;;A port of vim-commentary

(package! vterm)                       ;;Fully featured terminal emulator
(package! ranger)
(package! undo-tree)

(package! treemacs)                    ;;A treestyle file explorer
(package! treemacs-all-the-icons)      ;;All-the-icons integration for treemacs
(package! treemacs-icons-dired)        ;;treemacs icons for dired
(package! treemacs-magit)              ;;Magit integration for treemacs
(package! treemacs-persp)              ;;Persp-mode integration for treemacs
(package! treemacs-projectile)         ;;Projectile integration for treemacs
(package! lsp-treemacs)
(package! treemacs-evil)

(package! dired-git)                   ;;Git integration for dired
(package! all-the-icons-dired)         ;;Shows icons for each file in dired mode
(package! dired-subtree)
(package! dired-k)

(package! magit)                       ;;A git porcelain insde emacs

(package! doom-modeline)               ;;A minimal and modern mode-line
(package! doom-themes)                 ;;an opinionated pack of modern color-theme



;; To install a package directly from a remote git repo, you must specify a
;; `:recipe'. You'll find documentation on what `:recipe' accepts here:
;; https://github.com/raxod502/straight.el#the-recipe-format
;(package! another-package
;  :recipe (:host github :repo "username/repo"))

;; If the package you are trying to install does not contain a PACKAGENAME.el
;; file, or is located in a subdirectory of the repo, you'll need to specify
;; `:files' in the `:recipe':
;(package! this-package
;  :recipe (:host github :repo "username/repo"
;           :files ("some-file.el" "src/lisp/*.el")))

;; If you'd like to disable a package included with Doom, you can do so here
;; with the `:disable' property:
;(package! builtin-package :disable t)

;; You can override the recipe of a built in package without having to specify
;; all the properties for `:recipe'. These will inherit the rest of its recipe
;; from Doom or MELPA/ELPA/Emacsmirror:
;(package! builtin-package :recipe (:nonrecursive t))
;(package! builtin-package-2 :recipe (:repo "myfork/package"))

;; Specify a `:branch' to install a package from a particular branch or tag.
;; This is required for some packages whose default branch isn't 'master' (which
;; our package manager can't deal with; see raxod502/straight.el#279)
;(package! builtin-package :recipe (:branch "develop"))

;; Use `:pin' to specify a particular commit to install.
;(package! builtin-package :pin "1a2b3c4d5e")


;; Doom's packages are pinned to a specific commit and updated from release to
;; release. The `unpin!' macro allows you to unpin single packages...
;(unpin! pinned-package)
;; ...or multiple packages
;(unpin! pinned-package another-pinned-package)
;; ...Or *all* packages (NOT RECOMMENDED; will likely break things)
;(unpin! t)



